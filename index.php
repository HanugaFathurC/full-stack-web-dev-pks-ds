<?php 

trait  Hewan {
  public $nama ;
  public $darah  ;
  public $jumlahKaki ;
  public $keahlian ;

  public function profil($nama, $jumlahKaki, $keahlian,  $darah = 50){
    $this -> nama = $nama ;
    $this -> darah = $darah ;
    $this -> jumlahKaki = $jumlahKaki ;
    $this -> keahlian = $keahlian;
  }

  public function atraksi(){
    return $this->nama . " sedang " . $this->keahlian  ;
  }

}

trait Fight  {
  use Hewan ;
  public $attackPower ;
  public $defencePower ;

  public function status($attackPower, $defencePower) {
    $this -> attackPower = $attackPower ;
    $this -> defencePower = $defencePower ;
  }

  public function serang( $target){
    return $this-> nama . " sedang menyerang " . $target . "<br>" ;

  }

  public function diserang($attack){
    return $this -> nama . " sedang diserang. " . "Darah sekarang : " . $this -> darah - $attack / $this -> defencePower  . "<br> ";
  }



}


class Elang  {
  use Hewan, Fight;
  public function getInfoHewan(){
  
    return $this->nama . " Darah " . $this -> darah. " ,Jumlah Kaki " . $this->jumlahKaki. " ,Keahlian " . $this->keahlian. " ,Attack Power  " . $this->attackPower. " ,Defencer Power " . $this -> defencePower . "<br>" ;
  }
}

class Harimau  {
  use Hewan, Fight;
  public function getInfoHewan(){
 
    return $this->nama . " Darah " . $this -> darah. " ,Jumlah Kaki " . $this->jumlahKaki. " ,Keahlian " . $this->keahlian. " ,Attack Power  " . $this->attackPower. " ,Defencer Power " . $this -> defencePower . "<br>" ;
  }
}



$elang = new Elang() ;
echo $elang -> profil('Elang', 2 , "terbang tinggi") ;
echo $elang->status(10, 10) ;
echo $elang -> serang('harimau') ;
echo $elang -> diserang(7) ; 
echo $elang -> getInfoHewan();

$harimau = new Harimau() ;
echo $harimau -> profil('Harimau', 4 , 'Lari cepat') ;
echo $harimau -> status(7, 8) ;
echo $harimau -> serang('elang') ;
echo $harimau -> diserang(10) ; 
echo $harimau -> getInfoHewan();
 





?>

